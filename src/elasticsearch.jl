#request post processing (if not done during indexing)
function analyze_english(c::ElasticSearchClient.Client, index::String, content::String)
	params = Dict{String, Any}()
	params["text"] = content
	params["analyzer"] = "english"
	response = post(string(c.connection_string, index, "/_analyze"), data = JSON.json(params))
	json(response)
end

#convert result of analysis with language analyzer
function convert_es_search_results(found_documents; remove_stopwords = false, remove_numbers=true, stop_words_file = "data/stopwords/stop_snowball.txt", dynamic_data=true, analyze_index=false, index="nyt", client::ElasticSearchClient.Client=ElasticSearchClient.Client("localhost", 9200))
	remove_words = String[]
	if remove_stopwords
		remove_words = read_stop_word_file(stop_words_file)
	end
	if dynamic_data
		df = Dates.DateFormat("y-m-dTH:M:S.sZ")
		c = DynamicData.DynamicDataSet{Document, DateTime}()
	else c = DocumentList() end

	lexicon = Lexicon()
	iter = 0
	for result in found_documents
		iter += 1
		source = result["_source"]
		if analyze_index
			tokens = analyze_english(client, index, source["content"])["tokens"]
			if remove_numbers filter!(t->t["type"]!="<NUM>", tokens) end
			words = map(t->t["token"], tokens)
		else
			#get rid of punctuation (except hyphens)
			content = replace(source["content"],r"[^[:^punct:]\-]","")
			if remove_numbers
				#remove any numbers if applicable
				content = replace(content, r" [[:digit:]]+", "")
			end
			content = replace(content,['\n','\t']," ")
			content = lowercase(content)
			words = split(content)
		end
		d = Document()
		if dynamic_data
			date = DateTime(source["publicationDate"], df)
			#make sure datapoint is assigned id
			dp = DynamicData.add(c, d, date, false)
			t_id = DynamicData.get_timestamp_index(c, date)
		else push!(c, d) end
		for w in words
			w = strip(w)
			#TODO check for stopword
			if length(w) == 0 || w ∈ remove_words
				continue
			end
			w_obj = addword(lexicon, w)
			# if dynamic_data
			# 	push!(w_obj.used_in_times, t_id)
			# end
			if haskey(d,w_obj)
				d[w_obj] += 1
			else
				d[w_obj] = 1
				w_obj.doc_freq += 1
				push!(w_obj.used_in_docs,dp)
			end
		end
		if iter%1000==0 println("processed $iter of $(length(found_documents)) documents") end
	end
	if dynamic_data	DynamicData.rebuild_indexes(c) end
	Corpus(c, lexicon)
end

function convert_es_term_vectors(client::ElasticSearchClient.Client, index::String, doc_ids::Array{String,1}, document_name::String, content_field_name::String, date_field_name::String; dynamic_data=true, remove_stopwords=false, stop_words_file="data/stopwords/stop_snowball.txt")
	if dynamic_data docs = DynamicData.DynamicDataSet{Document, DateTime}()
	else docs = DocumentList() end
	lexicon = Lexicon()
	c = Corpus(docs, lexicon)
	convert_es_term_vectors!(client, index, doc_ids, document_name, content_field_name, date_field_name, c, dynamic_data=dynamic_data, remove_stopwords = remove_stopwords, stop_words_file = stop_words_file)
end

function convert_es_term_vectors!(client::ElasticSearchClient.Client, index::String, doc_ids::Array{String,1}, document_name::String, content_field_name::String, date_field_name::String, c::CorpusUtils.Corpus; dynamic_data=true, remove_stopwords = false, stop_words_file = "data/stopwords/stop_snowball.txt")
	date_requests = 0
	stop_word_lookup = 0
	vector_lookup = 0
	remove_words = String[]
	if remove_stopwords
		remove_words = read_stop_word_file(stop_words_file)
	end
	if dynamic_data
		df = Dates.DateFormat("y-m-dTH:M:S.sZ")
		# get all dates
		tic()
		pds = ElasticSearchClient.get_documents_by_id(client, index, document_name, doc_ids, date_field_name)
		date_requests += toq()
	end

	tic()
	term_vectors = ElasticSearchClient.get_document_content_vectors_by_id(client, index, document_name, doc_ids)
	vector_lookup += toq()
	for (i,id) in enumerate(doc_ids)
		d = Document()
		if dynamic_data
			date = DateTime(pds[i]["fields"][date_field_name][1], df)
			#make sure datapoint is assigned id
			dp = DynamicData.add(c.documents, d, date, false)
			t_id = DynamicData.get_timestamp_index(c.documents, date)
		else push!(c.documents, d) end
		if haskey(term_vectors[i]["term_vectors"], content_field_name)
			cur_terms = term_vectors[i]["term_vectors"][content_field_name]["terms"]
		else
			println("WARN: document $id has no field $content_field_name")
			continue
		end
		terms = keys(cur_terms)
		for w in terms
			# w = strip(term)
			# if length(w) == 0	continue end
			if remove_stopwords
				tic()
				is_stopword = w ∈ remove_words
				stop_word_lookup += toq()
				if is_stopword continue end
			end
			w_obj = addword(c.lexicon, w)
			# if dynamic_data
			# 	push!(w_obj.used_in_times, t_id)
			# end
			tf = cur_terms[w]["term_freq"]
			d[w_obj] = tf
			# add one already during addword
			w_obj.freq += (tf - 1)
			w_obj.doc_freq += 1
			push!(w_obj.used_in_docs,dp)
		end
		if i%1000==0 println("processed $i of $(length(doc_ids)) documents") end
	end
	println("term vector request: $vector_lookup s, date request: $date_requests s, stop word lookups: $stop_word_lookup s")
	if dynamic_data	DynamicData.rebuild_indexes(c.documents) end
	c
end
