type Word
	id::Int64
	value::AbstractString
	freq::Int64
	doc_freq::Int64
	used_in_docs::Vector{Any}
	function Word(id, value)
		new(id, value, 0, 0, Vector{Any}())
	end
end

typealias Document Dict{Word,Int64}
typealias DocumentList Array{Document,1}
typealias DateList Array{DateTime, 1}

type Lexicon
	words::Vector{Word}
	string_dict::Dict{AbstractString, Int64}
	function Lexicon()
		new(Array{Word}(0), Dict{String, Word}())
	end
	function Lexicon{T<:AbstractString}(strings::Vector{T})
		l = Lexicon()
		for (i, s) in enumerate(strings)
			w = Word(i, s)
			push!(l.words,w)
			l.string_dict[s] = i
		end
		l
	end
end

type Corpus{T<:Union{DocumentList, DynamicData.DynamicDataSet{Document, DateTime}}}
	documents::T
	lexicon::Lexicon
end
